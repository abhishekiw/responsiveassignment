import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GridpageComponent } from './gridpage.component';

describe('GridpageComponent', () => {
  let component: GridpageComponent;
  let fixture: ComponentFixture<GridpageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GridpageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GridpageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
