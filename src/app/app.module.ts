import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from "@angular/router";
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { HeaderComponent } from './header/header.component';
import { MainNavComponent } from './main-nav/main-nav.component';
import { Section1Component } from './section1/section1.component';
import { GalleryComponent } from './gallery/gallery.component';
import { Section3Component } from './section3/section3.component';
import { Section4Component } from './section4/section4.component';
import { Section5Component } from './section5/section5.component';
import { Section6Component } from './section6/section6.component';
import { FooterComponent } from './footer/footer.component';
import { HomepageComponent } from './homepage/homepage.component';
import { GridpageComponent } from './gridpage/gridpage.component';
//routing
const routes: Routes = [
  {path : "header", component: HeaderComponent },
  {path: "main-nav", component:MainNavComponent},
  {path: "homepage", component: HomepageComponent}, 
  {path: "section6", component:Section6Component},
  {path: "footer", component:FooterComponent},
  {path: "gridpage", component:GridpageComponent}
]


@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    HeaderComponent,
    MainNavComponent,
    Section1Component,
    GalleryComponent,
    Section3Component,
    Section4Component,
    Section5Component,
    Section6Component,
    FooterComponent,
    HomepageComponent,
    GridpageComponent
    
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    AngularFontAwesomeModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
